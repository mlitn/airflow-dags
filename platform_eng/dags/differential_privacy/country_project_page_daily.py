from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator
from mergedeep import merge
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.operators.spark import SparkSubmitOperator, SparkSqlOperator
from wmf_airflow_common.partitions_builder import daily_partitions
from platform_eng.config.dag_config import (
    default_args,
    artifact
)

dag_id = 'country_project_page_daily_dag'
var_props = VariableProperties(f'{dag_id}_config')
source_granularity = '@hourly'
ds = '{{data_interval_start.format("YYYY-MM-DD")}}'
year = '{{data_interval_start.year}}'
month = '{{data_interval_start.month}}'
day = '{{data_interval_start.day}}'
source_table = 'wmf.pageview_actor'
venv = 'differential-privacy-0.1.0.conda.tgz'

modified_args = merge(
    default_args,
    {
        # Custom job settings, ~20% cluster resources
        # See https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Regular_jobs for normal cluster reference
        # (24GB exec mem + 4GB exec mem overhead) * 24 instances + (10GB driver mem * 2 instances) = 692GB
        'driver_cores': 2,
        'driver_memory': '10G',
        'executor_cores': 12,
        'executor_memory': '24G',
        'conf': {
            'spark.dynamicAllocation.maxExecutors': '24',
            'spark.sql.shuffle.partitions': 288, # 24 instances * 12 cores / instance = 288 2GB partitions
            'spark.sql.sources.partitionOverwriteMode': 'dynamic',
            'spark.executor.memoryOverhead': 4096,
            'spark.sql.warehouse.dir': '/tmp',
        },
    }
)

with DAG(
    dag_id=dag_id,
    start_date=var_props.get_datetime('start_date', datetime(2023, 2, 18)),
    schedule='@daily',
    default_args=modified_args,
    max_active_runs=1,
    tags=['spark', 'data_release', 'differential_privacy']
) as dag:

    s = NamedHivePartitionSensor(
        task_id='wait_for_pageview_actor',
        partition_names=daily_partitions(table=source_table, granularity=source_granularity),
        poke_interval=timedelta(minutes=60).total_seconds(),
        timeout=timedelta(hours=10).total_seconds()
    )

    conda_env = var_props.get('conda_env', artifact(venv))
    args = [year, month, day]
    do_dp_pageview_actor = SparkSubmitOperator.for_virtualenv(
        task_id="do_dp_pageview_actor",
        virtualenv_archive=conda_env,
        use_virtualenv_spark=True,
        entry_point='lib/python3.7/site-packages/differential_privacy/country_project_page_gaussian.py',
        launcher='skein',
        application_args=args,
        env_vars={"SPARK_CONF_DIR": "/etc/spark3/conf"},
        deploy_mode='cluster'
    )

    tmp_directory = var_props.get('tmp_directory', f'hdfs:///tmp/country_project_page/{ds}')
    pub_file = var_props.get('pub_file', f'hdfs:///wmf/data/published/datasets/country_project_page/{ds}.tsv')

    join_titles = SparkSqlOperator(
        task_id="join_titles",
        sql=var_props.get(
            'hql_path',
            'https://gitlab.wikimedia.org/repos/security/differential-privacy/-/raw/main/differential_privacy/hql/join_titles.hql'
        ),
        query_parameters={
            'destination_directory': tmp_directory,
            'year': year,
            'month': month,
            'day': day,
            'coalesce_partitions': 1
        },
    )

    clean_up = BashOperator(
        task_id="clean_up",
        bash_command=f"hdfs dfs -mv {tmp_directory}/*.csv {pub_file} && hdfs dfs -chmod +r {pub_file} && hdfs dfs -rm -r {tmp_directory}"
    )

    s >> do_dp_pageview_actor >> join_titles >> clean_up
