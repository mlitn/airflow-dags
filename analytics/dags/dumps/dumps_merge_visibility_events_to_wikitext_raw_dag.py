"""
************
NOTICE: This job is work in progress. If it fails, do please ignore.
************

Transferring dumps data to the Iceberg hourly dataset
Extracting data from the event.mediawiki_revision_visibility_change dataset
(on an hourly partition)
Writing to Iceberg  to update rows if rows are matched on
rev_id and meta.id and if rows are not matched, it ignores

This job waits for the existence of a event.mediawiki_revision_visibility_change hive partition.
The Hive script runs in Spark here is a link to the job

gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/merge_into_with_mwrvc.py
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import artifact, create_easy_dag, dataset
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

props = DagProperties(
    start_date=datetime(2023, 9, 12, 0),
    conda_env=artifact(
        "mediawiki-content-dump-0.1.0.dev0-T344698-Change-Revision-Visibility-Code-to-new-schema.conda.tgz"
    ),
    hive_wikitext_raw_table="wmf_dumps.wikitext_raw_rc1",
    hive_revision_visibility_change="event.mediawiki_revision_visibility_change",
    driver_memory="8G",
    executor_memory="8G",
    executor_cores="4",
    max_executors="64",
    sla=timedelta(hours=6),
)
with create_easy_dag(
    dag_id="dumps_merge_visibility_events_to_wikitext_raw",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@hourly",
    tags=[
        "hourly",
        "from_hive",
        "to_iceberg",
        "requires_mediawiki_revision_visibility_change",
        "uses_spark",
        "mediawiki_dumps",
    ],
    sla=props.sla,
    max_active_runs=1,  # MERGEs will step into each other, let's run them serially.
    email="jebe@wikimedia.org",  # overriding alert email for now.
    # it's ok if this fails, and we don't want to alert ops week folk.
) as dag:
    sensor = dataset("hive_event_mediawiki_revision_visibility_change").get_sensor_for(dag)

    args = [
        props.hive_revision_visibility_change,
        props.hive_wikitext_raw_table,
        "{{data_interval_start.year}}",
        "{{data_interval_start.month}}",
        "{{data_interval_start.day}}",
        "{{data_interval_start.hour}}",
    ]

    merge_into = SparkSubmitOperator.for_virtualenv(
        task_id="spark_merge_into",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/mediawiki_content_dump/merge_into_with_mwrvc.py",
        driver_memory=props.driver_memory,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf={
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
            "spark.driver.extraJavaOptions": "-Xss4m",
        },
        launcher="skein",
        application_args=args,
    )

    sensor >> merge_into
