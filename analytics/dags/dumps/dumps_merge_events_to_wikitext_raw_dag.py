"""
************
NOTICE: This job is work in progress. If it fails, do please ignore.
************

This job consumes event data from event.mediawiki_page_content_change_v1 and merges it into an
Iceberg table containing all existing (wki_db, revisions) tuples. This target table is meant to be
an intermediary table to generate dumps from.

We accomplish this by running a pyspark job that runs a MERGE INTO
that de-duplicates events and mutates the target table.

More info about the pyspark job at:
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/events_merge_into.py
"""
import getpass
from datetime import datetime, timedelta

from analytics.config.dag_config import artifact, create_easy_dag, dataset
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

props = DagProperties(
    # DAG settings
    start_date=datetime(2023, 8, 23, 0),
    sla=timedelta(hours=6),
    conda_env=artifact("mediawiki-content-dump-0.1.0.dev0-fix-tmp-location.conda.tgz"),
    hive_wikitext_raw_table="wmf_dumps.wikitext_raw_rc1",
    hive_mediawiki_page_content_change_table="event.mediawiki_page_content_change_v1",
    # Spark job tuning
    driver_memory="16G",
    driver_cores="4",
    executor_memory="16G",
    executor_cores="2",
    num_executors="64",
    spark_driver_maxResultSize="8G",
    # this job runs Spark 3.3. Disable shuffle service for now as 3.1's is incompatible
    spark_shuffle_service_enabled="False",
    spark_dynamicAllocation_enabled="False",
)


with create_easy_dag(
    dag_id="dumps_merge_events_to_wikitext_raw",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@hourly",
    tags=[
        "hourly",
        "from_hive",
        "to_iceberg",
        "requires_mediawiki_page_content_change_v1",
        "uses_spark",
        "mediawiki_dumps",
    ],
    sla=props.sla,
    max_active_runs=1,  # MERGEs will step into each other, let's run them serially.
    email="xcollazo@wikimedia.org"  # overriding alert email for now.
    # it's ok if this fails, and we don't want to alert ops week folk.
) as dag:
    sensor = dataset("hive_event_mediawiki_page_content_change_v1").get_sensor_for(dag)

    # Usage: merge_into.py source_table, target_table, year, month, day, hour
    args = [
        props.hive_mediawiki_page_content_change_table,
        props.hive_wikitext_raw_table,
        "{{data_interval_start.year}}",
        "{{data_interval_start.month}}",
        "{{data_interval_start.day}}",
        "{{data_interval_start.hour}}",
    ]
    username = getpass.getuser()  # avoid ivy errors by making cache location unique per service user
    merge_into = SparkSubmitOperator.for_virtualenv(
        task_id="spark_merge_into",
        virtualenv_archive=props.conda_env,
        entry_point="bin/events_merge_into.py",
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        num_executors=props.num_executors,
        conf={
            "spark.driver.maxResultSize": props.spark_driver_maxResultSize,
            "spark.shuffle.service.enabled": props.spark_shuffle_service_enabled,
            "spark.dynamicAllocation.enabled": props.spark_dynamicAllocation_enabled,
            "spark.jars.packages": "org.apache.iceberg:iceberg-spark-runtime-3.3_2.12:1.2.1",
            "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
            "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",  # fix jar pulling
            "spark.yarn.archive": "hdfs:///user/xcollazo/artifacts/spark-3.3.2-assembly.zip",  # override 3.1's assembly
        },
        launcher="skein",
        application_args=args,
        use_virtualenv_spark=True,
        default_env_vars={
            "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
            "SPARK_CONF_DIR": "/etc/spark3/conf",
        },
    )

    sensor >> merge_into
