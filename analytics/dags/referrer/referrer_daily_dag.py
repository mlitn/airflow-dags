"""
* This job moves referer data from wmf.pageview_actor
* to the wmf.referrer_daily table and archives referrer data.

Note
* This job reads from the pageview_actor table
* and runs the compute_referer_daily.hql query
* which loads the wmf.referrer table
* and then runs the compute_referer_archive_daily.hql
* which drops the result into a temporary folder
  temporary folder /wmf/tmp/analytics/referrer-,
* and then archived into /wmf/data/archive/referrer/daily/referrals-for-{date}
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    archive_directory,
    dataset,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "referrer_daily"
var_props = VariableProperties(f"{dag_id}_config")
referrer_daily_table = "wmf.referrer_daily"
referrer_daily_table_iceberg = "wmf_traffic.referrer_daily"


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 3, 1)),
    schedule="@daily",
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=6),
        },
    ),
    user_defined_filters=filters,
    tags=[
        "daily",
        "from_hive",
        "to_hdfs",
        "to_hive",
        "to_iceberg",
        "uses_archiver",
        "uses_hql",
        "requires_wmf_pageview_actor",
    ],
) as dag:
    sensor = dataset("hive_wmf_pageview_actor").get_sensor_for(dag)

    temporary_directory = var_props.get(
        "temporary_directory",
        f"{hadoop_name_node}/wmf/tmp/analytics/referrer-{dag_id}/{{{{data_interval_start|to_ds_nodash}}}}",
    )

    run_referer_hql = SparkSqlOperator(
        task_id="run_referer_hql",
        sql=var_props.get("run_referer_hql_path", f"{hql_directory}/referrer/compute_referer_daily.hql"),
        query_parameters={
            "source_table": var_props.get("source_table", "wmf.pageview_actor"),
            "coalesce_partitions": var_props.get("coalesce_partitions", 1),
            "referer_daily_destination_table": var_props.get("referer_daily_destination_table", referrer_daily_table),
            "min_num_daily_referrals": var_props.get("min_num_daily_referrals", 500),
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
    )

    run_referer_hql_iceberg = SparkSqlOperator(
        task_id="run_referer_hql_iceberg",
        sql=var_props.get(
            "run_referer_hql_iceberg_path", f"{hql_directory}/referrer/compute_referer_daily_iceberg.hql"
        ),
        query_parameters={
            "source_table": var_props.get("source_table", "wmf.pageview_actor"),
            "coalesce_partitions": var_props.get("coalesce_partitions", 1),
            "referer_daily_destination_table": var_props.get(
                "referer_daily_destination_table_iceberg", referrer_daily_table_iceberg
            ),
            "min_num_daily_referrals": var_props.get("min_num_daily_referrals", 500),
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
    )

    run_referer_archive_hql = SparkSqlOperator(
        task_id="run_referer_archive_hql",
        sql=var_props.get(
            "run_referer_archive_hql_path", f"{hql_directory}/referrer/compute_referer_archive_daily.hql"
        ),
        query_parameters={
            "referer_archive_source_table": var_props.get("referer_archive_source_table", referrer_daily_table),
            "destination_directory": temporary_directory,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
    )

    move_referer_data_to_archive = HDFSArchiveOperator(
        task_id="move_referer_data_to_archive",
        source_directory=temporary_directory,
        archive_file=var_props.get(
            "archive_file", f"{archive_directory}/referrer/daily/referrals-for-{{{{data_interval_start|to_ds}}}}.tsv"
        ),
        expected_filename_ending=".csv",
        check_done=True,
    )

    sensor >> [run_referer_hql, run_referer_hql_iceberg] >> run_referer_archive_hql >> move_referer_data_to_archive
