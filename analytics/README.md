# Airflow DAG Tags Naming Convention

The proposed tags to be used in the WMF Airflow instances.

## Schedule

We want to be able to group tasks by their schedule, suggestions for these are:
- monthly
- weekly
- daily
- hourly


## Sensor/Requiring a table/artifact

We want to be able to group dags by what table or artifact they require to run
in the formart requires_dbname_tablename common examples are:

- requires_wmf_webrequest
- requires_wmf_pageview_actor
- requires_wmf_mediawiki_page


## Mechanisms used within the DAG

Depending on if the dag has/uses hql or spark submit operators we want to be able to identify 
these dags as follows:
- uses_hql -- used for spark operator 
- uses_spark -- used with the spark submit operator and druid operator
- uses_archiver
- uses_skein


## Source and Destination Databases

Tagging sources and destination for the dags in this format:
- to_druid
- to_cassandra
- to_iceberg
- to_hive
- from_hive
- from_iceberg
