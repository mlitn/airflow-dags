#!/bin/bash

set -e

# Script description: Launch commands to setup or run our project.

function process_argument() {
    # Process the argument and call the appropriate function based on its value
    case $1 in
        build)
            build_environment
            ;;
        install_precommit)
            install_precommit
            ;;
        lint)
            lint
            ;;
        test)
            unit_tests
            ;;
        reset_fixtures)
            reset_fixtures
            ;;
        test_debug)
            unit_tests_debug
            ;;
        format)
            format_code
            ;;
        docker_test)
            run_unit_tests_from_docker_image
            ;;
        docker_lint)
            run_lint_from_docker_image
            ;;
        *)
            echo "Invalid argument. Please provide a valid argument."
            exit 1
            ;;
    esac
}

# This scripts assumes that the name of the environment is your current folder name.
CONDA_ENV_NAME=$(basename "$(pwd)")

if [ ! "$(git rev-parse --show-toplevel 2>/dev/null)" = "$PWD" ]; then
    echo "Please cd to the root of your airflow-dags repo."
fi

eval "$(conda shell.bash hook)"

function build_environment() {
    # Function to build the environment
    conda activate base
    conda env remove --name ${CONDA_ENV_NAME}
    conda env create --name ${CONDA_ENV_NAME} -f conda-environment.lock.yml
    conda activate ${CONDA_ENV_NAME}
    pip install ".[test,dev,lint]"
    # Install Dockerfile linting tool
    if [[ $OSTYPE == 'linux-gnu' && $(uname -m) == 'x86_64' ]] ; then
      if [ ! -f "build/hadolint" ]; then
          mkdir -p build
          wget --quiet -O build/hadolint https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64
          chmod +x build/hadolint
      fi
    elif [[ $PLATFORM == 'Darwin' ]]; then
      brew list hadolint || brew install hadolint
    else
      echo "Please install hadolint manually."
    fi
    #  install pre-commit to trigger formatting & linting tools when a developer runs `git commit`
    echo "Optionally to run autoformatting + linting tools before commit, run: pre-commit install"
}

function lint() {
    # Function to run linting checks
    conda activate ${CONDA_ENV_NAME}
    flake8 && printf "flake8 OK\n\n"
    mypy && printf "mypy OK\n\n"
    black --check . && printf "black OK\n\n"
    isort --check . && printf "isort OK\n\n"
    hadolint --config debian/hadolint.yml --failure-threshold warning debian/Dockerfile && echo "hadolint OK"
}

function unit_tests() {
    # Function to run unit tests
    conda activate ${CONDA_ENV_NAME}
    pytest
}

function unit_tests_debug() {
    # Function to run unit tests in debug mode
    conda activate ${CONDA_ENV_NAME}
    pytest -svv
}

function reset_fixtures() {
    # Function to reset fixtures
    find tests -regex 'tests/.*/fixtures/.*' -type f -delete
    conda activate ${CONDA_ENV_NAME}
    REBUILD_FIXTURES=yes pytest
}

function format_code() {
    # Function to auto format the code
    conda activate ${CONDA_ENV_NAME}
    isort .
    black .
}

function docker_test() {
    # Function to run unit tests inside a Docker container
    DOCKER_BUILDKIT=1 \
      docker build \
      --platform linux/x86_64 \
      -m 8g -c 5 \
      -f debian/Dockerfile \
      --target conda_env_test -t conda_env_test .
    docker run \
      --platform linux/x86_64 \
      --memory="8g" \
      --cpus="5.0" \
      --rm -it \
      --volume .:/opt/airflow-dags \
      --workdir /opt/airflow-dags \
      conda_env_test \
      bash -c -p "source /opt/miniconda/bin/activate && conda activate airflow && pytest"
}

function docker_lint() {
    # Function to run linting checks inside a Docker container
    DOCKER_BUILDKIT=1 \
      docker build \
      --platform linux/x86_64 \
      -m 8g -c 5 \
      -f debian/Dockerfile \
      --target conda_env_lint -t conda_env_lint .

    docker run \
      --platform linux/x86_64 \
      --memory="8g" \
      --cpus="5.0" \
      --rm -it \
      --volume .:/opt/airflow-dags \
      --workdir /opt/airflow-dags \
      conda_env_lint \
      bash -c -p "source /opt/miniconda/bin/activate && conda activate airflow && flake8 && mypy && black --check . && isort --check ."
}

# Parse command-line arguments
if [ $# -ne 1 ]; then
    echo "Invalid number of arguments. Please provide exactly one argument."
    exit 1
fi

process_argument $1