import pytest

# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['platform_eng', 'dags', 'differential_privacy', 'geoeditors.py']


def test_country_project_page_daily_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="geoeditors_dag")
    assert dag is not None
    assert len(dag.tasks) == 7
